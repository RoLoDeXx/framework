<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Credit Entity
 *
 * @property int $id
 * @property int|null $author_id
 * @property int|null $artifact_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property string|null $comments
 * @property string|null $credit_to
 *
 * @property \App\Model\Entity\Author $author
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Credit extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author_id' => true,
        'artifact_id' => true,
        'date' => true,
        'comments' => true,
        'credit_to' => true,
        'author' => true,
        'artifacts' => true
    ];
}
