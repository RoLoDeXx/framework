<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MaterialAspect Entity
 *
 * @property int $id
 * @property string|null $material_aspect
 *
 * @property \App\Model\Entity\ArtifactsMaterial[] $artifacts_materials
 */
class MaterialAspect extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material_aspect' => true,
        'artifacts_materials' => true
    ];
}
