<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->allowEmpty('username');

        $validator
            ->scalar('created_by')
            ->maxLength('created_by', 255)
            ->allowEmpty('created_by');

        $validator
            ->boolean('admin')
            ->allowEmpty('admin');

        $validator
            ->integer('download_hd_images')
            ->allowEmpty('download_hd_images');

        $validator
            ->scalar('filtering')
            ->maxLength('filtering', 255)
            ->allowEmpty('filtering');

        $validator
            ->boolean('can_download_hd_images')
            ->allowEmpty('can_download_hd_images');

        $validator
            ->boolean('can_view_private_catalogues')
            ->allowEmpty('can_view_private_catalogues');

        $validator
            ->boolean('can_view_private_transliterations')
            ->allowEmpty('can_view_private_transliterations');

        $validator
            ->boolean('can_edit_transliterations')
            ->allowEmpty('can_edit_transliterations');

        $validator
            ->boolean('can_view_private_images')
            ->allowEmpty('can_view_private_images');

        $validator
            ->boolean('can_view_iPadWeb')
            ->allowEmpty('can_view_iPadWeb');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('password')
            ->lengthBetween('password', [8, 128])
            ->allowEmpty('password');

        $validator
            ->scalar('2fa_key')
            ->maxLength('2fa_key', 255)
            ->allowEmpty('2fa_key');

        $validator
            ->boolean('2fa_status')
            ->allowEmpty('2fa_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));

        return $rules;
    }
}
